Add the following records to the blog_db database:
 
 INSERT INTO users (email, password, datetime_created) VALUES
    ->          ("johnsmith@gmail.com", "passwordA", '2021-01-01 01:00:00'),
    ->          ("juandelacruz@gmail.com", "passwordB", '2021-01-01 02:00:00'),
    ->          ("janesmith@gmail.com", "passwordC", '2021-01-01 03:00:00'),
    ->          ("mariadelacruz@gmail.com", "passwordD", '2021-01-01 04:00:00'),
    ->          ("johndoe@gmail.com", "passwordE", '2021-01-01 05:00:00');



MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES
    ->         (1, "First Code", "Hello World!", '2021-01-02 01:00:00'),
    ->         (1, "Second Code", "Hello Earth", '2021-01-02 02:00:00'),
    ->         (2, "Third Code", "Welcome to Mars!", '2021-01-02 03:00:00'),
    ->         (4, "Fourth Code", "Bye bye solar system!", '2021-01-02 04:00:00');


MariaDB [blog_db]> SELECT * FROM posts where author_id = 1;
+----+-----------+-------------+--------------+---------------------+
| id | author_id | title       | content      | datetime_posted     |
+----+-----------+-------------+--------------+---------------------+
|  1 |         1 | First Code  | Hello World! | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth  | 2021-01-02 02:00:00 |
+----+-----------+-------------+--------------+---------------------+


MariaDB [blog_db]> SELECT email, datetime_created FROM users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelacruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+



MariaDB [blog_db]>  UPDATE posts
    ->       SET content = "Hello to the people of the Earth!"
    ->       WHERE content = "Hello Earth" AND id = 2;


MariaDB [blog_db]> DELETE FROM users WHERE email = "johndoe@gmail.com";
